CREATE TABLE IF NOT EXISTS client (
    id BIGINT NOT NULL,
    first_name VARCHAR(255) NOT NULL,
    last_name VARCHAR(255) NOT NULL,
    patronymic VARCHAR(255) NOT NULL,
    birthday date NOT NULL,
    passport_id BIGINT,
    CONSTRAINT pk_client PRIMARY KEY (id));

INSERT INTO client (id, first_name, last_name, patronymic, birthday, passport_id)
VALUES (1,'Ivan', 'Ivanov' , 'Ivanovich', '10/11/1990', 1),
       (2,'Michail', 'Popov' , 'Vasilevich', '17/12/1959', 2);

CREATE TABLE IF NOT EXISTS passport (
    id BIGINT NOT NULL,
    serial_number INTEGER NOT NULL,
    passport_number INTEGER NOT NULL,
    date_issue date NOT NULL,
    client_id BIGINT,
    CONSTRAINT pk_passport PRIMARY KEY (id),
    CONSTRAINT fk_client FOREIGN KEY(client_id)
    REFERENCES client (id)
    );


INSERT INTO passport (id, serial_number, passport_number, date_issue, client_id)
VALUES (1,3355, 334576 , '10/12/2014', 1),
       (2,4477, 321789 , '01/09/1991', 2);

ALTER TABLE client ADD CONSTRAINT fk_passport FOREIGN KEY (passport_id) REFERENCES passport(id);

CREATE TABLE IF NOT EXISTS transfer (
    id BIGINT NOT NULL,
    sender VARCHAR(255) NOT NULL,
    recipient VARCHAR(255) NOT NULL,
    amount BIGINT NOT NULL,
    transfer_date date NOT NULL,
    client_id BIGINT NOT NULL,
    CONSTRAINT pk_transfer PRIMARY KEY (id),
    CONSTRAINT fk_client_transfer FOREIGN KEY (client_id) REFERENCES client(id)
    );

INSERT INTO transfer (id, sender, recipient, transfer_date, amount, client_id)
VALUES (1,'Ivan Ivanov Ivanovich', 'Michail Popov Vasilevich' , '10/11/2022', 100, 1),
       (2,'Ivan Ivanov Ivanovich', 'Michail Popov Vasilevich' , '11/11/2022', 200, 1),
       (3,'Michail Popov Vasilevich', 'Ivan Ivanov Ivanovich' , '01/09/2021', 100, 2),
       (4,'Michail Popov Vasilevich', 'Ivan Ivanov Ivanovich' , '10/12/2022', 500, 2),
       (5,'Michail Popov Vasilevich', 'Ivan Ivanov Ivanovich' , '22/12/2022', 1000, 2);