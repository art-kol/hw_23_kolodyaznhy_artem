package ru.hw.hw23.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.hw.hw23.entity.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

}
