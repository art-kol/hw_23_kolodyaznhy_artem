package ru.hw.hw23.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.hw.hw23.entity.Passport;

@Repository
public interface PassportRepository extends JpaRepository<Passport, Long> {
    Passport findBySerialNumberAndPassportNumber(Integer serialNumber, Integer passportNumber);
}
