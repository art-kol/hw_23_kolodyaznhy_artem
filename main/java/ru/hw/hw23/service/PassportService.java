package ru.hw.hw23.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.hw.hw23.dto.ClientUIDto;
import ru.hw.hw23.entity.Transfer;
import ru.hw.hw23.mapping.ClientConverter;
import ru.hw.hw23.repository.PassportRepository;

import java.util.List;

@Service
public class PassportService {

    @Autowired
    private ClientConverter clientConverter;

    @Autowired
    private PassportRepository passportRepository;

    public ClientUIDto getClientBySerialNumberAndPassportNumber(Integer serialNumber, Integer passportNumber) {
        return clientConverter.convertClientToClientDUDto(passportRepository.
                findBySerialNumberAndPassportNumber(serialNumber, passportNumber).getClient());
    }

    public List<Transfer> getAllTransfersClient(Integer serialNumber, Integer passportNumber) {
        return passportRepository.
                findBySerialNumberAndPassportNumber(serialNumber,passportNumber)
                .getClient().getTransfers();
    }

}
