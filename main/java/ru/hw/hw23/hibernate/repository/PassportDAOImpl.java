package ru.hw.hw23.hibernate.repository;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.hw.hw23.dto.ClientUIDto;
import ru.hw.hw23.entity.Passport;
import ru.hw.hw23.entity.Transfer;
import ru.hw.hw23.hibernate.dao.PassportDAO;
import ru.hw.hw23.mapping.ClientConverter;
import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class PassportDAOImpl implements PassportDAO {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private ClientConverter clientConverter;

    @Override
    public ClientUIDto getClientBySerialNumberAndPassportNumber
            (Integer serialNumber, Integer passportNumber) {

        Session currentSession = entityManager.unwrap(Session.class);
        Query<Passport> query = currentSession.createQuery
                ("from Passport where serialNumber = :paramSerial and passportNumber =: paramNumber", Passport.class);
        query.setParameter("paramSerial", serialNumber);
        query.setParameter("paramNumber", passportNumber);
        return clientConverter.convertClientToClientDUDto(query.getSingleResult().getClient());
    }

    @Override
    public List<Transfer> getAllTransfersClient(Integer serialNumber, Integer passportNumber){
        Session currentSession = entityManager.unwrap(Session.class);
        Query<Passport> query = currentSession.createQuery
                ("from Passport where serialNumber = :paramSerial and passportNumber =: paramNumber", Passport.class);
        query.setParameter("paramSerial", serialNumber);
        query.setParameter("paramNumber", passportNumber);
        return query.getSingleResult().getClient().getTransfers();
    }

}
