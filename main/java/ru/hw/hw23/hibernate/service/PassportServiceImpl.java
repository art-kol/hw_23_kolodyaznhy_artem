package ru.hw.hw23.hibernate.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.hw.hw23.dto.ClientUIDto;
import ru.hw.hw23.entity.Transfer;
import ru.hw.hw23.hibernate.dao.PassportDAO;
import java.util.List;

@Service
public class PassportServiceImpl implements PassportService{

    @Autowired
    private PassportDAO passportDAO;

    @Transactional
    @Override
    public ClientUIDto getClientBySerialNumberAndPassportNumber
            (Integer serialNumber, Integer passportNumber) {
        return passportDAO.getClientBySerialNumberAndPassportNumber(serialNumber, passportNumber);
    }

    @Transactional
    @Override
    public List<Transfer> getAllTransfersClient(Integer serialNumber, Integer passportNumber) {
        return passportDAO.getAllTransfersClient(serialNumber, passportNumber);
    }

}
