package ru.hw.hw23.hibernate.service;

import ru.hw.hw23.dto.ClientUIDto;
import ru.hw.hw23.entity.Transfer;

import java.util.List;

public interface PassportService {

    ClientUIDto getClientBySerialNumberAndPassportNumber(Integer serialNumber, Integer passportNumber);

    List<Transfer> getAllTransfersClient(Integer serialNumber, Integer passportNumber);


}
