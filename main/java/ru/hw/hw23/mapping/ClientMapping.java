package ru.hw.hw23.mapping;

import org.springframework.stereotype.Service;
import ru.hw.hw23.dto.ClientUIDto;
import ru.hw.hw23.entity.Client;

@Service
public class ClientMapping {

    public ClientUIDto mapToClientUI(Client client){
        ClientUIDto clientUIDto = new ClientUIDto();
        clientUIDto.setId(client.getId());
        clientUIDto.setFirstName(client.getFirstName());
        clientUIDto.setLastName(client.getLastName());
        clientUIDto.setPatronymic(client.getPatronymic());
        clientUIDto.setBirthday(client.getBirthday());
        return clientUIDto;
    }

}
