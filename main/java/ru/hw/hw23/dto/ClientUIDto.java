package ru.hw.hw23.dto;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import java.sql.Date;

@Data
@Getter
@Setter
public class ClientUIDto {

    private Long id;
    private String firstName;
    private String lastName;
    private String patronymic;
    private Date birthday;

}
