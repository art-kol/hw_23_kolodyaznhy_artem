package ru.hw.hw23.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.hw.hw23.dto.ClientUIDto;
import ru.hw.hw23.entity.Transfer;
import ru.hw.hw23.hibernate.service.PassportServiceImpl;
import ru.hw.hw23.service.PassportService;

import java.util.List;

@RestController
public class MainController {

    @Autowired
    private PassportService passportService;

    @Autowired
    private PassportServiceImpl passportServiceImpl;

    @GetMapping
    @RequestMapping("/transfers")
    public ResponseEntity<List<Transfer>> getAllTransfersByPassport(
            @RequestParam(name = "serial") Integer serialNumber,
            @RequestParam(name = "passport") Integer passportNumber) {
        return new ResponseEntity<>(passportService.
                getAllTransfersClient(serialNumber,passportNumber),
                HttpStatus.OK);
    }

    @GetMapping
    @RequestMapping("/client")
    public ResponseEntity<ClientUIDto> getClientByPassport(
            @RequestParam(name = "serial") Integer serialNumber,
            @RequestParam(name = "passport") Integer passportNumber) {
        return new ResponseEntity<>(passportService.
                getClientBySerialNumberAndPassportNumber(serialNumber,passportNumber),
                HttpStatus.OK);
    }

    @GetMapping
    @RequestMapping("/transfers_hibernate")
    public ResponseEntity<List<Transfer>> getAllTransfersByPassport_hibernate(
            @RequestParam(name = "serial") Integer serialNumber,
            @RequestParam(name = "passport") Integer passportNumber) {
        return new ResponseEntity<>(passportService.
                getAllTransfersClient(serialNumber,passportNumber),
                HttpStatus.OK);
    }

    @GetMapping
    @RequestMapping("/client_hibernate")
    public ResponseEntity<ClientUIDto> getClientByPassport_hibernate(
            @RequestParam(name = "serial") Integer serialNumber,
            @RequestParam(name = "passport") Integer passportNumber) {
        return new ResponseEntity<>(passportServiceImpl.
                getClientBySerialNumberAndPassportNumber(serialNumber, passportNumber),
                HttpStatus.OK);
    }

}
