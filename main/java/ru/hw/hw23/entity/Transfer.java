package ru.hw.hw23.entity;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import java.sql.Date;

@Entity
@Getter
@Setter
@Table(name = "transfer")
public class Transfer {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "sender")
    private String sender;

    @Column(name = "recipient")
    private String recipient;

    @Column(name = "amount")
    private Long amount;

    @Column(name = "transfer_date")
    private Date transferDate;


}
