package ru.hw.hw23.entity;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import java.sql.Date;

@Entity
@Getter
@Setter
@Table(name = "passport")
public class Passport {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "serial_number")
    private Integer serialNumber;

    @Column(name = "passport_number")
    private Integer passportNumber;

    @Column(name = "date_issue")
    private Date dateIssue;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "client_id")
    private Client client;

}